package com.example.exercise3;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

public class Utilisation implements LifecycleObserver {
    private static int nbUse=0;

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void NombreUtilisation(){
        nbUse++;
    }

    public static int getNbUtilisation(){
        return nbUse;
    }

}
