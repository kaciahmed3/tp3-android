package com.example.exercise3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Random;

public class Inscription extends AppCompatActivity {
    EditText edLastName;
    EditText edFirstName;
    EditText edAge;
    EditText edPhone;
    private String ID;
    public static final String EXTRA_LASTNAME ="LastName";
    public static final String EXTRA_FIRSTNAME ="FirstName";
    public static final String EXTRA_AGE ="Age";
    public static final String EXTRA_PHONE ="Phone";
    public static final String EXTRA_ID ="ID";
    public static final String EXTRA_FILE_NAME ="FileName";
    FileOutputStream fOut = null;
    OutputStreamWriter osw = null;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inscription);
        context = getApplicationContext();
        if (savedInstanceState == null) {
            Toast.makeText(this, getString(R.string.stateCreated), Toast.LENGTH_SHORT).show();
            ID = getID();
        } else {
            ID=savedInstanceState.getString(EXTRA_ID);
            Toast.makeText(this, getString(R.string.stateRecovered), Toast.LENGTH_SHORT).show();
        }
        getLifecycle().addObserver(new Utilisation());
    }
        @Override
        protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
            super.onRestoreInstanceState(savedInstanceState);
            Toast.makeText(this, getString(R.string.stateRestored), Toast.LENGTH_SHORT).show();
        }

    // méthode pour la génération de l'identifiant
        private String getID() {
            Random rand =new Random();
            Integer x =rand.nextInt();
            x=Math.abs(x);
            return x.toString();
        }

    // méthode qui permet la sauvegarde des informations de l'utilisateur dans un fichier.
        public void writeFile(){
            edLastName=(EditText)findViewById(R.id.edLastName);
            edFirstName=(EditText)findViewById(R.id.edFirstName);
            edAge=(EditText)findViewById(R.id.edAge);
            edPhone=(EditText)findViewById(R.id.edPhone);

            try{
                fOut = context.openFileOutput(edLastName.getText().toString()+""+ID,MODE_PRIVATE);
                osw = new OutputStreamWriter(fOut);

                osw.write(EXTRA_LASTNAME+" : "+edLastName.getText().toString()+"\n");
                osw.write(EXTRA_FIRSTNAME+" : "+edFirstName.getText().toString()+"\n");
                osw.write(EXTRA_AGE+" : "+edAge.getText().toString()+"\n");
                osw.write(EXTRA_PHONE+" : "+edPhone.getText().toString()+"\n");
                osw.write(EXTRA_ID+" : "+ID+"\n");
                osw.write(getString(R.string.nbUses)+" : "+Utilisation.getNbUtilisation()+"\n");
                osw.flush();
                Toast.makeText(context, getString(R.string.strSettingSaved),Toast.LENGTH_SHORT).show();
            }
            catch (Exception e) {
                Toast.makeText(context, getString(R.string.strSettingNotSaved),Toast.LENGTH_SHORT).show();
            }
            finally {
                try {
                    osw.close();
                    fOut.close();
                } catch (IOException e) {
                    Toast.makeText(context, getString(R.string.fileClosureError),Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        protected void onSaveInstanceState(@NonNull Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putString(EXTRA_ID,ID);
            Toast.makeText(this, getString(R.string.stateSaved), Toast.LENGTH_SHORT).show();

        }
    // méthode qui permet de lancer une deuxième activité qui affiche les informations sauvegardées dans le fichier.
        public void onSubmit(View view){
            edLastName=(EditText)findViewById(R.id.edLastName);
            Intent intent1 =new Intent(context,ActivityShowInformations.class);
            writeFile();
            String filename = edLastName.getText().toString() + "" + ID;
            intent1.putExtra(EXTRA_FILE_NAME, filename);
            startActivity(intent1);
        }

        @Override
        protected void onDestroy() {
            super.onDestroy();
            getLifecycle().removeObserver(new Utilisation());
            Toast.makeText(this, getString(R.string.stateDestroyed), Toast.LENGTH_SHORT).show();
        }
        // redéfinition de la méthode onResume pour lui ajouter un observeur (la classe Utilisation)
        @Override
        protected void onResume() {
            super.onResume();
        }
}