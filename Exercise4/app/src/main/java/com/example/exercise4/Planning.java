package com.example.exercise4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;


import android.os.Bundle;
import android.widget.TextView;

public class Planning extends AppCompatActivity {
    public static final String EXTRA_SLOT1="08h-10h";
    public static final String EXTRA_SLOT2="10h-12h";
    public static final String EXTRA_SLOT3="14h-16h";
    public static final String EXTRA_SLOT4="16h-18h";
    TextView tvSlot1;
    TextView tvSlot2;
    TextView tvSlot3;
    TextView tvSlot4;
    PlanningModel planningModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planning);

        planningModel =new ViewModelProvider(this).get(PlanningModel.class);

        tvSlot1=(TextView)findViewById(R.id.tvSlots1);
        tvSlot1.setText(EXTRA_SLOT1+" : "+planningModel .getInfoSlot1());
        tvSlot2=(TextView)findViewById(R.id.tvSlots2);
        tvSlot2.setText(EXTRA_SLOT2+" : "+planningModel .getInfoSlot2());
        tvSlot3=(TextView)findViewById(R.id.tvSlots3);
        tvSlot3.setText(EXTRA_SLOT3+" : "+planningModel .getInfoSlot3());
        tvSlot4=(TextView)findViewById(R.id.tvSlots4);
        tvSlot4.setText(EXTRA_SLOT4+" : "+planningModel .getInfoSlot4());

    }
}