package com.example.exercise4;

import androidx.lifecycle.ViewModel;

public class PlanningModel extends ViewModel {
    private String infoSlot1;
    private String infoSlot2;
    private String infoSlot3;
    private String infoSlot4;

    public PlanningModel() {
        this.setInfoSlot1("Rencontre client Dupont");
        this.setInfoSlot2("Travailler le dossier recrutement");
        this.setInfoSlot3("Réunion équipe");
        this.setInfoSlot4("Préparation dossier vente");
    }

    public String getInfoSlot1() {
        return infoSlot1;
    }

    public void setInfoSlot1(String infoSlot1) {
        this.infoSlot1 = infoSlot1;
    }

    public String getInfoSlot2() {
        return infoSlot2;
    }

    public void setInfoSlot2(String infoSlot2) {
        this.infoSlot2 = infoSlot2;
    }

    public String getInfoSlot3() {
        return infoSlot3;
    }

    public void setInfoSlot3(String infoSlot3) {
        this.infoSlot3 = infoSlot3;
    }

    public String getInfoSlot4() {
        return infoSlot4;
    }

    public void setInfoSlot4(String infoSlot4) {
        this.infoSlot4 = infoSlot4;
    }
}
