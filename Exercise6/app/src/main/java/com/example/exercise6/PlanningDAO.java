package com.example.exercise6;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;


@Dao
public interface PlanningDAO {

    @Insert
    void insertPlanning(PlanningEntity planningEntity);

    @Query("SELECT * FROM TablePlanning where planningID=:id")
    PlanningEntity getPlanning(int id);

}
