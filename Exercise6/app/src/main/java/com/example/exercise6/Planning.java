package com.example.exercise6;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class Planning extends AppCompatActivity {
    public static final String EXTRA_SLOT1="08h-10h";
    public static final String EXTRA_SLOT2="10h-12h";
    public static final String EXTRA_SLOT3="14h-16h";
    public static final String EXTRA_SLOT4="16h-18h";
    TextView tvSlot1;
    TextView tvSlot2;
    TextView tvSlot3;
    TextView tvSlot4;
    PlanningModel planningModel;

    FileOutputStream fOut = null;
    OutputStreamWriter osw = null;
    Context context;
    public static final String EXTRA_FILE_NAME ="planning";

    InscriptionViewModel inscriptionViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planning);
        context=getApplicationContext();
        // écriture du fichier planning
        writeFile();
        planningModel= new ViewModelProvider(this).get(PlanningModel.class);
        planningModel.runTimer(context);
        subscribe();
    // utilisé pour sauvegarder les informations d'inscrption de l'utilisateur qui a été affecté un planning dans la bdd
        inscriptionViewModel = new ViewModelProvider(this).get(InscriptionViewModel.class);
    }
    // methode qui permet de mettre à jour l'affichage du planning à chaque fois que le planning modèle change.
    public void subscribe(){
        final Observer<PlanningModel> planningModelObserver=new Observer<PlanningModel>() {
            @Override
            public void onChanged(PlanningModel planningModel2) {
                tvSlot1=(TextView)findViewById(R.id.tvSlots1);
                tvSlot1.setText(EXTRA_SLOT1+" : "+planningModel2.getInfoSlot1());
                tvSlot2=(TextView)findViewById(R.id.tvSlots2);
                tvSlot2.setText(EXTRA_SLOT2+" : "+planningModel2.getInfoSlot2());
                tvSlot3=(TextView)findViewById(R.id.tvSlots3);
                tvSlot3.setText(EXTRA_SLOT3+" : "+planningModel2.getInfoSlot3());
                tvSlot4=(TextView)findViewById(R.id.tvSlots4);
                tvSlot4.setText(EXTRA_SLOT4+" : "+planningModel2.getInfoSlot4());
            }
        };
        planningModel.getNewPlanningModel().observe(this,planningModelObserver);
    }
    // méthode qui permet d'écrire le fichier planning
    public void writeFile(){
        try{
            fOut = context.openFileOutput(EXTRA_FILE_NAME,MODE_PRIVATE);
            osw = new OutputStreamWriter(fOut);

            osw.write(EXTRA_SLOT1+" : Rencontre client Dupont \n");
            osw.write(EXTRA_SLOT2+" : Travailler le dossier recrutement \n");
            osw.write(EXTRA_SLOT3+" : Réunion équipe \n");
            osw.write(EXTRA_SLOT4+" : Préparation dossier vente \n");

            osw.flush();
            Toast.makeText(context, getString(R.string.strSettingSaved),Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            Toast.makeText(context, getString(R.string.strSettingNotSaved),Toast.LENGTH_SHORT).show();
        }
        finally {
            try {
                osw.close();
                fOut.close();
            } catch (IOException e) {
                Toast.makeText(context, getString(R.string.fileClosureError),Toast.LENGTH_SHORT).show();
            }
        }
    }
    // méthode pour sauvegarder les informations d'inscription et le planning dans la BDD
    public void saveInDataBase(View view){
        // sauvegarde du planning dans la BDD
        PlanningEntity planningEntity =new PlanningEntity(1,"Rencontre client Dupont","Travailler le dossier recrutement","Réunion équipe","Préparation dossier vente");

        planningModel.insertPlanning(planningEntity);

        // sauvegarde dans la bdd, des informations d'inscrption de l'utilisateur qui possède le planning.
        Intent intent=getIntent();
        InscriptionEntity inscriptionEntity =new InscriptionEntity();
        inscriptionEntity.setID(Integer.parseInt(intent.getStringExtra(Inscription.EXTRA_ID)));
        inscriptionEntity.setLastName(intent.getStringExtra(Inscription.EXTRA_LASTNAME));
        inscriptionEntity.setFirstName(intent.getStringExtra(Inscription.EXTRA_FIRSTNAME));
        inscriptionEntity.setAge(intent.getStringExtra(Inscription.EXTRA_AGE));
        inscriptionEntity.setPhone(intent.getStringExtra(Inscription.EXTRA_PHONE));
        inscriptionEntity.setPlanningID(planningEntity.getPlanningID());

        inscriptionViewModel.insertInscription(inscriptionEntity);
        Toast.makeText(getApplicationContext(),getString(R.string.msgSaveInDb),Toast.LENGTH_LONG).show();
    }
    // méthode pour mettre à jour le planning Modèle à partir de la BDD
    public void updatePlanningModelFromDb(View view){
        planningModel.updatePlanningModelFromDb();
        Toast.makeText(getApplicationContext(),getString(R.string.msgUpdateFromDb),Toast.LENGTH_LONG).show();
    }
    // méthode pour retourner à l'activité d'inscription
    public void onReturn(View view){
        Planning.this.finish();
    }
}