package com.example.exercise6;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Random;

@Entity(tableName="TablePlanning")
public class PlanningEntity {

    @PrimaryKey
    private int planningID;

    private String slot1;
    private String slot2;
    private String slot3;
    private String slot4;

    public PlanningEntity() {
    }

    public PlanningEntity(int ID,String slot1, String slot2, String slot3, String slot4) {
        this.slot1 = slot1;
        this.slot2 = slot2;
        this.slot3 = slot3;
        this.slot4 = slot4;
        this.planningID=ID;
    }

    public int getPlanningID() {
        return planningID;
    }

    public void setPlanningID(int planningID) {
        this.planningID = planningID;
    }

    public String getSlot1() {
        return slot1;
    }

    public void setSlot1(String slot1) {
        this.slot1 = slot1;
    }

    public String getSlot2() {
        return slot2;
    }

    public void setSlot2(String slot2) {
        this.slot2 = slot2;
    }

    public String getSlot3() {
        return slot3;
    }

    public void setSlot3(String slot3) {
        this.slot3 = slot3;
    }

    public String getSlot4() {
        return slot4;
    }

    public void setSlot4(String slot4) {
        this.slot4 = slot4;
    }
}
