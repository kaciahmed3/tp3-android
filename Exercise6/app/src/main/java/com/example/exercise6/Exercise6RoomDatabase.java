package com.example.exercise6;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {InscriptionEntity.class,PlanningEntity.class}, version = 1, exportSchema = false)
public abstract class  Exercise6RoomDatabase extends RoomDatabase{

        public abstract InscriptionDAO inscriptionDAO();
        public abstract PlanningDAO planningDAO();

        private static volatile Exercise6RoomDatabase databaseInstance;

        static Exercise6RoomDatabase getDatabase(final Context context){
            if (databaseInstance==null){
                synchronized (Exercise6RoomDatabase.class){
                    if(databaseInstance==null){
                        databaseInstance= Room.databaseBuilder(context.getApplicationContext(),Exercise6RoomDatabase.class,"Exercise6Database").allowMainThreadQueries().build();
                    }
                }
            }
            return databaseInstance;
        }
}
