package com.example.exercise6;

import android.app.Application;
import android.database.CursorJoiner;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public class InscriptionViewModel extends AndroidViewModel {
    InscriptionDAO inscriptionDAO;
    Exercise6RoomDatabase database;
    public InscriptionViewModel(@NonNull Application application) {
        super(application);
        // récupération de l'insatnce de la BDD
        database= Exercise6RoomDatabase.getDatabase(application);
        //récupération de l'insatance du DAO
        inscriptionDAO=database.inscriptionDAO();
    }
    // méthode qui permet l'insertion des informations de l'inscription dans la base de donnée
    public void insertInscription(InscriptionEntity inscriptionEntity){
        new InsertAsyncTask(inscriptionDAO).execute(inscriptionEntity);

    }
    // classe qui permet d'effectuer l'insertion en arrière plans
    private class InsertAsyncTask extends AsyncTask<InscriptionEntity,Void,Void>{
        InscriptionDAO mInscriptionDao;

        public InsertAsyncTask(InscriptionDAO mInscriptionDao) {
            this.mInscriptionDao = mInscriptionDao;
        }

        @Override
        protected Void doInBackground(InscriptionEntity... inscriptionEntities) {
            mInscriptionDao.insertInscription(inscriptionEntities[0]);
            return null;
        }
    }
}
