package com.example.exercise6;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface InscriptionDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertInscription(InscriptionEntity inscriptionEntity);

    @Query("SELECT * FROM TableInscription where id =:id")
    LiveData<InscriptionEntity> getInscription( int id);

}
