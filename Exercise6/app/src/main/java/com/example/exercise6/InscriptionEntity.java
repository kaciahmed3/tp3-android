package com.example.exercise6;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName="TableInscription", foreignKeys = @ForeignKey(entity = PlanningEntity.class,
        parentColumns = "planningID",
        childColumns = "planningID"))
public class InscriptionEntity {
    @PrimaryKey
    private int ID;
    private String lastName;
    private String firstName;
    private String age;
    private String phone;
    private int planningID;

    public InscriptionEntity() {
    }

    public InscriptionEntity(int ID, String lastName, String firstName, String age, String phone,int planningID) {
        this.ID = ID;
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
        this.phone = phone;
        this.planningID=planningID;
    }

    public int getID() {
        return ID;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getAge() {
        return age;
    }

    public String getPhone() {
        return phone;
    }

    public int getPlanningID() {
        return planningID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPlanningID(int planningID) {
        this.planningID = planningID;
    }
}
