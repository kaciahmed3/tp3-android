package com.example.exercise6;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class PlanningModel extends AndroidViewModel {
    private String infoSlot1;
    private String infoSlot2;
    private String infoSlot3;
    private String infoSlot4;
    public static final String EXTRA_FILE_NAME ="planning";

    PlanningDAO planningDAO;
    Exercise6RoomDatabase database;

    private MutableLiveData<PlanningModel> planningSend = new MutableLiveData<>();

    //constructeur
    public PlanningModel(@NonNull Application application) {
        super(application);
        // récupération de l'insatnce de la BDD
        database= Exercise6RoomDatabase.getDatabase(application);
        //récupération de l'insatance du DAO
        planningDAO=database.planningDAO();
    }
    // méthode pour lire le fichier planning
    public void ReadFile(Context context) {
        FileInputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader bufferedReader;
        String tabInfo[];
        final String EXTRA_SLOT1="08h-10h";
        final String EXTRA_SLOT2="10h-12h";
        final String EXTRA_SLOT3="14h-16h";
        final String EXTRA_SLOT4="16h-18h";
        try {
            fIn = context.openFileInput(EXTRA_FILE_NAME);
            isr = new InputStreamReader(fIn);
            bufferedReader = new BufferedReader(isr);
            String data = null;
            while ((data = bufferedReader.readLine()) != null) {
                tabInfo=data.split(":");
                switch (tabInfo[0].trim()){
                    case EXTRA_SLOT1:{
                        this.setInfoSlot1(tabInfo[1]);
                        break;
                    } case EXTRA_SLOT2: {
                        this.setInfoSlot2(tabInfo[1]);
                        break;
                    }case EXTRA_SLOT3: {
                        this.setInfoSlot3(tabInfo[1]);
                        break;
                    }case EXTRA_SLOT4: {
                        this.setInfoSlot4(tabInfo[1]);
                        break;
                    }
                }
            }
            planningSend.postValue(this);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                isr.close();
                fIn.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    // méthode qui utlise un thread pour lire le fichier toute les 10 secondes
    // (j'ai trouver cette méthode dans le livre headFirst et je l'ai adapter au contexte de l'exercice 5)
    public void runTimer(Context context){
        final Handler handler=new Handler();
        handler.post(new Runnable() {
                         @Override
                         public void run() {
                             ReadFile(context);
                             Log.e("entity", "runner");
                             handler.postDelayed(this::run,10000);
                         }
                     }
        );
    }
    // getters et setters
    public String getInfoSlot1() {
        return infoSlot1;
    }

    public void setInfoSlot1(String infoSlot1) {
        this.infoSlot1 = infoSlot1;
    }

    public String getInfoSlot2() {
        return infoSlot2;
    }

    public void setInfoSlot2(String infoSlot2) {
        this.infoSlot2 = infoSlot2;
    }

    public String getInfoSlot3() {
        return infoSlot3;
    }

    public void setInfoSlot3(String infoSlot3) {
        this.infoSlot3 = infoSlot3;
    }

    public String getInfoSlot4() {
        return infoSlot4;
    }

    public void setInfoSlot4(String infoSlot4) {
        this.infoSlot4 = infoSlot4;
    }

    // méthode qui retourne un objet liveData qui envlope l'objet planning mis à jour.
    public LiveData<PlanningModel> getNewPlanningModel(){
        return planningSend;
    }

    // méthode qui permet l'insertion du planning dans la base de donnée
    public void insertPlanning(PlanningEntity planningEntity){
        new PlanningModel.InsertAsyncTask(planningDAO).execute(planningEntity);
    }
    // classe qui permet d'effectuer l'insertion en arrière plans
    private class InsertAsyncTask extends AsyncTask<PlanningEntity,Void,Void>{
        PlanningDAO mPlanningDAO;

        public InsertAsyncTask(PlanningDAO mPlanningDAO) {
            this.mPlanningDAO = mPlanningDAO;
        }

        @Override
        protected Void doInBackground(PlanningEntity... planningEntities) {
            mPlanningDAO.insertPlanning(planningEntities[0]);
            return null;
        }
    }
    //méthode qui permet la mise a jour du planning à partir de la bdd
    public void updatePlanningModelFromDb(){
        PlanningEntity planningEntity=planningDAO.getPlanning(1);
        if(planningEntity!=null){
            this.setInfoSlot1(planningEntity.getSlot1());
            this.setInfoSlot2(planningEntity.getSlot2());
            this.setInfoSlot3(planningEntity.getSlot3());
            this.setInfoSlot4(planningEntity.getSlot4());
        }
    }
}
