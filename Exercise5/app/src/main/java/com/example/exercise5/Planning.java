package com.example.exercise5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class Planning extends AppCompatActivity {
    public static final String EXTRA_SLOT1="08h-10h";
    public static final String EXTRA_SLOT2="10h-12h";
    public static final String EXTRA_SLOT3="14h-16h";
    public static final String EXTRA_SLOT4="16h-18h";
    TextView tvSlot1;
    TextView tvSlot2;
    TextView tvSlot3;
    TextView tvSlot4;
    PlanningModel planningModel;

    FileOutputStream fOut = null;
    OutputStreamWriter osw = null;
    Context context;
    public static final String EXTRA_FILE_NAME ="planning";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planning);
        context=getApplicationContext();
        // écriture du fichier planning
        writeFile();
        planningModel =new ViewModelProvider(this).get(PlanningModel.class);
        planningModel.runTimer(context);
        subscribe();
    }
    // methode qui permet de mettre à jour l'affichage du planning à chaque fois que le planning modèle change.
    public void subscribe(){
        final Observer<PlanningModel> planningModelObserver=new Observer<PlanningModel>() {
            @Override
            public void onChanged(PlanningModel planningModel2) {
                tvSlot1=(TextView)findViewById(R.id.tvSlots1);
                tvSlot1.setText(EXTRA_SLOT1+" : "+planningModel2.getInfoSlot1());
                tvSlot2=(TextView)findViewById(R.id.tvSlots2);
                tvSlot2.setText(EXTRA_SLOT2+" : "+planningModel2.getInfoSlot2());
                tvSlot3=(TextView)findViewById(R.id.tvSlots3);
                tvSlot3.setText(EXTRA_SLOT3+" : "+planningModel2.getInfoSlot3());
                tvSlot4=(TextView)findViewById(R.id.tvSlots4);
                tvSlot4.setText(EXTRA_SLOT4+" : "+planningModel2.getInfoSlot4());
                Toast.makeText(context,getString(R.string.msgUpdateFromFile),Toast.LENGTH_LONG).show();
            }
        };
        planningModel.getNewPlanningModel().observe(this,planningModelObserver);
    }
    // méthode qui permet d'écrire le fichier planning
    public void writeFile(){
        try{
            fOut = context.openFileOutput(EXTRA_FILE_NAME,MODE_PRIVATE);
            osw = new OutputStreamWriter(fOut);

            osw.write(EXTRA_SLOT1+" : Rencontre client Dupont \n");
            osw.write(EXTRA_SLOT2+" : Travailler le dossier recrutement \n");
            osw.write(EXTRA_SLOT3+" : Réunion équipe \n");
            osw.write(EXTRA_SLOT4+" : Préparation dossier vente \n");

            osw.flush();
            Toast.makeText(context, getString(R.string.strSettingSaved),Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            Toast.makeText(context, getString(R.string.strSettingNotSaved),Toast.LENGTH_SHORT).show();
        }
        finally {
            try {
                osw.close();
                fOut.close();
            } catch (IOException e) {
                Toast.makeText(context, getString(R.string.fileClosureError),Toast.LENGTH_SHORT).show();
            }
        }
    }
}