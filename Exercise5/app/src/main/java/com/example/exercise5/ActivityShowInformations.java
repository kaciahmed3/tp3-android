package com.example.exercise5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ActivityShowInformations extends AppCompatActivity {
    LinearLayout ll;
    TextView tv;
    FileInputStream fIn = null;
    InputStreamReader isr = null;
    BufferedReader bufferedReader;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_informations);
        ll = (LinearLayout)findViewById(R.id.linearLayout1);
        context=getApplicationContext();
        ReadFile();

    }
    // méthode pour lire les informations d'inscrption à partir du fichier
    public void ReadFile(){
        Intent intent =getIntent();
        String fileName = intent.getStringExtra(Inscription.EXTRA_FILE_NAME);

        try{
            fIn = openFileInput(fileName);
            isr = new InputStreamReader(fIn);
            bufferedReader=new BufferedReader(isr);
            String data = null;
            while ((data =bufferedReader.readLine())!=null){
                tv=new TextView(context);
                tv.setText(data);
                tv.setTextSize(20);
                tv.setTextColor(Color.BLACK);
                ll.addView(tv);
            }
        }
        catch (Exception e) {
            Toast.makeText(context, getString(R.string.strSettingsNotRecovered),Toast.LENGTH_SHORT).show();
        }
        finally {
            try {
                isr.close();
                fIn.close();
            } catch (IOException e) {
                Toast.makeText(context, getString(R.string.fileClosureError),Toast.LENGTH_SHORT).show();
            }
        }
    }
    // méthode pour retourner à l'activité d'inscription
    public void onReturn(View view){
        ActivityShowInformations.this.finish();
    }
}