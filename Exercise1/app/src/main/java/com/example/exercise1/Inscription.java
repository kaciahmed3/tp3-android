package com.example.exercise1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class Inscription extends AppCompatActivity {

    private String ID;
    public static final String EXTRA_ID ="ID";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inscription);
        if(savedInstanceState ==null) {
            Toast.makeText(this, getString(R.string.stateCreated),Toast.LENGTH_SHORT).show();
            ID = getID();
        }else{
            ID=savedInstanceState.getString(EXTRA_ID);
            Toast.makeText(this, getString(R.string.stateRecovered), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Toast.makeText(this, getString(R.string.stateRestored), Toast.LENGTH_SHORT).show();
    }
// méthode pour la génération de l'identifiant
    private String getID(){
        Random rand =new Random();
        Integer x =rand.nextInt();
        x=Math.abs(x);
        return x.toString();
    }

// sauvegarde de l'identifiant quelque soit l'état de l'activité.
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("ID",ID);
        Toast.makeText(this, getString(R.string.stateSaved), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, getString(R.string.stateDestroyed), Toast.LENGTH_SHORT).show();
    }
}